exports.paths = {
	watched: ['src']
};

exports.files = {
	javascripts: {
		joinTo: {
			'assets/site.js': [
				/^node_modules/,
				/^src\/.+\.js/
			]
		},
	},
	stylesheets: {
		joinTo: 'assets/site.css'
	}
};

exports.plugins = {
	sass: {
		mode: 'native',
		compress: 'compressed',
		options: {
			includePaths: ['node_modules']
		}
	},
	postcss: {
		processors: [
			require('autoprefixer')(['defaults']),
			require('csswring')()
		]
	}
};

exports.npm = {
	globals: {
		// Uncomment these if you're using jQuery
		// jQuery: 'jquery',
		// $: 'jquery',
	}
}

exports.modules = {
	autoRequire: {
		'assets/site.js': ['src/site']
	}
};

exports.overrides = {
	production: {
		optimize: true,
		sourceMaps: false,
		paths: {
			public: 'build/web'
		}
	}
};
